module Model exposing (Model, addIngredientToInputRecipe, addInstructionToInputRecipe, addToRecipeList, emptyAllInput, emptyInputIngredient, emptyInputInstruction, emptyInputRecipe, emptyModal, giveID, giveIDs, increaseCounter, makeNewID, removeIngredient, removeInstruction, setImportedFile, setInputRecipe, setModal, setModalVisibility, updateRecipe)

import Html exposing (..)
import List.Extra
import Msg exposing (Msg, ViewMode)
import Recipe exposing (Recipe)


type alias Model =
    { recipeList : List Recipe
    , modalIsVisible : Bool
    , currentModal : Html Msg
    , inputRecipe : Recipe
    , inputInstruction : String
    , inputIngredient : String
    , idCounter : Int
    , exportedRecipe : String
    , importedFile : Maybe String
    , viewMode : ViewMode
    }



-- Model Pipe Functions


setImportedFile : String -> Model -> Model
setImportedFile imported model =
    { model | importedFile = Just imported }


setModal : Html Msg -> Model -> Model
setModal msg model =
    { model | currentModal = msg }


setInputRecipe : Recipe -> Model -> Model
setInputRecipe recipe model =
    { model | inputRecipe = recipe }


updateRecipe : Model -> Model
updateRecipe model =
    { model | recipeList = Recipe.changeRecipeWithSameID model.inputRecipe model.recipeList }


removeIngredient : String -> Model -> Model
removeIngredient ingredient model =
    let
        removeIngredientFromRecipe recipe ingr =
            { recipe | ingredients = List.Extra.remove ingr recipe.ingredients }
    in
    { model | inputRecipe = removeIngredientFromRecipe model.inputRecipe ingredient }


removeInstruction : String -> Model -> Model
removeInstruction instruction model =
    let
        removeInstructionFromRecipe recipe instr =
            { recipe | instructions = List.Extra.remove instr recipe.instructions }
    in
    { model | inputRecipe = removeInstructionFromRecipe model.inputRecipe instruction }


emptyAllInput : Model -> Model
emptyAllInput model =
    model
        |> emptyInputInstruction
        |> emptyInputIngredient
        |> emptyInputRecipe
        |> emptyInputInstruction
        |> emptyModal


emptyInputInstruction : Model -> Model
emptyInputInstruction model =
    { model | inputInstruction = "" }


addInstructionToInputRecipe : String -> Model -> Model
addInstructionToInputRecipe instruction model =
    case instruction of
        "" ->
            model

        _ ->
            { model | inputRecipe = Recipe.addInstruction instruction model.inputRecipe }


addIngredientToInputRecipe : String -> Model -> Model
addIngredientToInputRecipe ingredient model =
    { model | inputRecipe = Recipe.addIngredient ingredient model.inputRecipe }


emptyInputRecipe : Model -> Model
emptyInputRecipe model =
    { model
        | inputRecipe = Recipe.invalidRecipe
    }


emptyInputIngredient : Model -> Model
emptyInputIngredient model =
    { model | inputIngredient = "" }


setModalVisibility : Bool -> Model -> Model
setModalVisibility visibility model =
    { model
        | modalIsVisible = visibility
    }


emptyModal : Model -> Model
emptyModal model =
    { model | currentModal = div [] [] }


addToRecipeList : List Recipe -> List Recipe -> Model -> Model
addToRecipeList recipeList recipes model =
    case Recipe.validateRecipeList recipes of
        True ->
            { model | recipeList = (++) recipeList (giveIDs model recipes) } |> increaseCounter (List.length recipes)

        False ->
            model


giveIDs : Model -> List Recipe -> List Recipe
giveIDs model recipes =
    List.map
        (\recipe ->
            { recipe
                | id =
                    model.idCounter
                        + (case List.Extra.elemIndex recipe recipes of
                            Just a ->
                                a

                            Nothing ->
                                0
                          )
            }
        )
        recipes


giveID : Model -> Recipe -> ( Recipe, Model )
giveID model recipe =
    ( { recipe | id = model.idCounter }, model |> increaseCounter 1 )


increaseCounter : Int -> Model -> Model
increaseCounter int model =
    { model | idCounter = model.idCounter + int }


makeNewID : Model -> Model
makeNewID model =
    let
        changeID recipe newID =
            { recipe | id = newID }
    in
    { model | inputRecipe = changeID model.inputRecipe model.idCounter }
