module Msg exposing (Component(..), EditMode(..), Msg(..), RecipeInputType(..), ViewMode(..))

import File exposing (File)
import Html exposing (Html)
import Http exposing (Error)
import Recipe exposing (Recipe)


type Msg
    = CreateNewRecipe
    | Save EditMode Component
    | Delete EditMode Component
    | UpdateRecipeComponent RecipeInputType String
    | ToggleModalAppearance Bool
    | SetModelModal (Html Msg)
    | OpenRecipe Recipe
    | EditRecipe Recipe
    | CloseModal
    | DownloadRecipe Recipe
    | DownloadCollection
    | ImportRecipe
    | FileSelected File
    | FileLoaded String
    | OpenDeleteConfirmationModal Recipe
    | ToggleRecipeView ViewMode
    | GotText (Result Http.Error String)


type RecipeInputType
    = RecipeTitleInput
    | RecipePictureURIInput
    | InstructionInput
    | IngredientInput
    | ServingsInput



-- Info if modal is recipe creation or edit


type EditMode
    = ChangeOld
    | CreateNew


type Component
    = RecipeObject Recipe
    | Instruction String
    | Ingredient String


type ViewMode
    = CardView
    | ListView


type Visibility
    = Open
    | Closed
