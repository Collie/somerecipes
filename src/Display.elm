module Display exposing (createModal, createRecipeModal, deleteRecipeConfirmationModal, displayButtons, displayCreateRecipeButton, displayCurrentModal, displayDownloadCollectionButton, displayImportButton, displayIngredients, displayIngredientsInEdit, displayInstructions, displayInstructionsInEdit, displayNavbar, displayRecipeCard, displayRecipeCards, displayRecipeList, displayRecipeListItem, displayRecipeModal, displayRecipePicture, displayServingsAndCalories, inputField, onClickNoBubblingUp)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode
import Model exposing (Model)
import Msg exposing (..)
import Recipe exposing (..)



-- Modals


deleteRecipeConfirmationModal : Recipe -> Html Msg
deleteRecipeConfirmationModal recipe =
    let
        head =
            [ div [] [] ]

        body =
            [ p [] [ text "Do you really want to delete the recipe?" ] ]

        foot =
            [ button
                [ class "button is-danger"
                , onClick (Delete ChangeOld (RecipeObject recipe))
                ]
                [ text "Yeah delete!" ]
            , button [ class "button is-outlined is-primary", onClick CloseModal ]
                [ text "Nah, changed my mind" ]
            ]
    in
    createModal head body foot


createRecipeModal : Model -> EditMode -> Html Msg
createRecipeModal model editMode =
    let
        head =
            [ p [ class "modal-card-title" ]
                [ text
                    (case editMode of
                        CreateNew ->
                            "Create Recipe"

                        ChangeOld ->
                            "Edit Recipe"
                    )
                ]
            ]

        body =
            [ span [ class "field mylabel is-horizontal" ]
                [ div [ class "margin-right-05" ]
                    [ h1 [ class "subtitle" ] [ text "Title" ]
                    ]
                , inputField "Title" (UpdateRecipeComponent RecipeTitleInput) model.inputRecipe.title
                ]
            , span [ class "field mylabel is-horizontal" ]
                [ div [ class "margin-right-05" ]
                    [ h1 [ class "subtitle" ] [ text "Image-URI" ]
                    ]
                , inputField "Image-URI" (UpdateRecipeComponent RecipePictureURIInput) model.inputRecipe.pictureURI
                ]
            , span [ class "field mylabel is-horizontal" ]
                [ div [ class "margin-right-05" ]
                    [ h1 [ class "subtitle" ] [ text "Servings" ]
                    ]
                , inputField "Image-URI" (UpdateRecipeComponent ServingsInput) (String.fromInt model.inputRecipe.servings)
                ]
            , displayIngredientsInEdit editMode model.inputRecipe.ingredients
            , div [ class "field is-horizontal is-grouped margin-top-1", id "ingredientInput" ]
                [ inputField "New Ingredient" (UpdateRecipeComponent IngredientInput) ""
                , button [ class "margin-right-1", class "button", onClick (Save editMode (Ingredient model.inputIngredient)) ] [ i [ class " icon fas fa-plus" ] [] ]
                ]
            , displayInstructionsInEdit editMode model.inputRecipe.instructions
            , div [ class "field is-horizontal is-grouped margin-top-1", id "instructionInput" ]
                [ inputField "New Instruction" (UpdateRecipeComponent InstructionInput) ""
                , button [ class "margin-right-1", class "button", onClick (Save editMode (Instruction model.inputInstruction)) ] [ i [ class " icon fas fa-plus" ] [] ]
                ]
            ]

        foot =
            [ button
                [ class "button is-primary"
                , onClick (Save editMode (RecipeObject model.inputRecipe))
                ]
                [ text "Save Recipe" ]
            , button [ class "button is-outlined is-danger", onClick CloseModal ]
                [ text "Close without saving" ]
            ]
    in
    createModal head body foot


displayRecipeModal : Recipe -> Html Msg
displayRecipeModal recipe =
    let
        head =
            [ p [ class "modal-card-title" ]
                [ text recipe.title ]
            , button [ class "is-pulled-right margin-right-05 is-large button custom-background", onClick (DownloadRecipe recipe) ] [ i [ class "icon far fa-save" ] [] ]
            , button [ class "is-pulled-right margin-right-05 is-large button custom-background", onClick (EditRecipe recipe) ] [ i [ class "icon far fa-edit" ] [] ]
            , button [ class "is-pulled-right margin-right-05 is-large button custom-background", onClick (OpenDeleteConfirmationModal recipe) ] [ i [ class "icon far fa-trash-alt" ] [] ]
            , button [ class "is-large delete", onClick (ToggleModalAppearance False) ] []
            ]

        body =
            [ displayRecipePicture recipe
            , div [ class "margin-top-1" ]
                [ displayServingsAndCalories recipe.servings recipe.calorieCount
                , displayIngredients recipe.ingredients
                ]
            , displayInstructions recipe.instructions
            ]

        foot =
            [ button [ class "button is-danger", onClick (ToggleModalAppearance False) ]
                [ text "Close" ]
            ]
    in
    createModal head body foot


displayRecipeCards : List Recipe -> Html Msg
displayRecipeCards recipes =
    div [ class "columns is-multiline margin-top-1" ] (List.map displayRecipeCard recipes)


displayRecipeCard : Recipe -> Html Msg
displayRecipeCard recipe =
    div [ class "column is-one-fifth" ]
        [ div [ class "card" ]
            [ div [ class "card-content pointer-changed", onClick (OpenRecipe recipe) ]
                [ figure [ class "image is-3by2" ] [ img [ src recipe.pictureURI ] [] ]
                , p [ class "margin-all-05 has-text-weight-bold" ] [ text recipe.title ]
                , p [ class "margin-all-05" ] [ text ("Servings: " ++ String.fromInt recipe.servings) ]
                , p [ class "margin-all-05" ] [ text ("Calories: " ++ String.fromFloat (toFloat recipe.calorieCount / toFloat recipe.servings)) ]
                ]
            , div [ class "card-footer" ]
                [ div [ class "card-footer-item" ]
                    [ button [ class "button no-border", onClickNoBubblingUp (DownloadRecipe recipe) ] [ i [ class "icon far fa-save" ] [] ]
                    ]
                , div [ class "card-footer-item" ]
                    [ button [ class "button no-border", onClickNoBubblingUp (EditRecipe recipe) ] [ i [ class "icon far fa-edit" ] [] ]
                    ]
                , div [ class "card-footer-item" ]
                    [ button [ class "button no-border", onClickNoBubblingUp (OpenDeleteConfirmationModal recipe) ] [ i [ class "icon far fa-trash-alt" ] [] ]
                    ]
                ]
            ]
        ]



-- generic Input Field Maker


inputField : String -> (String -> Msg) -> String -> Html Msg
inputField name msg val =
    div [ class "control" ]
        [ input [ class "input margin-bottom-1", type_ "text", placeholder name, onInput msg, value val ] [] ]



-- generic Modal Maker


createModal : List (Html Msg) -> List (Html Msg) -> List (Html Msg) -> Html Msg
createModal head body foot =
    div [ class "modal is-active" ]
        [ div [ class "modal-background" ]
            []
        , div [ class "modal-card" ]
            [ header [ class "custom-modal-header modal-card-head" ]
                head
            , section [ class "modal-card-body" ]
                body
            , footer [ class "custom-modal-footer modal-card-foot" ]
                foot
            ]
        ]



-- viel-helper functions


displayNavbar : Html Msg
displayNavbar =
    nav [ class "navbar is-light is-fixed-top" ]
        [ div [ class "navbar-brand" ]
            [ img [ style "vertical-align" "middle", class "navbar-item", src "assets/images/Some-Recipes-logo.png", Html.Attributes.width 50, Html.Attributes.height 35 ] []
            , h1 [ class "navbar-item title is-4" ] [ text "Some Recipes" ]
            ]
        , div [ class "navbar-menu navbar-end" ]
            [ div [ class "navbar-item field" ]
                [ button [ class "button custom-background", onClick (ToggleRecipeView ListView) ] [ i [ class "icon fas fa-list" ] [] ]
                , button [ class "button custom-background margin-left-05", onClick (ToggleRecipeView CardView) ] [ i [ class "icon fas fa-th" ] [] ]
                ]
            ]
        ]


displayEdamamAttribution : Html Msg
displayEdamamAttribution =
    div [ id "edamam-badge", class "is-pulled-right" ] []


displayServingsAndCalories : Int -> Int -> Html Msg
displayServingsAndCalories servings calories =
    section [ class "columns" ]
        [ h1 [ class "subtitle column" ] [ text ("Servings: " ++ String.fromInt servings) ]
        , h1 [ class "subtitle margin-left-1 column" ] [ text ("Calories: " ++ String.fromFloat (toFloat calories / toFloat servings)) ]
        ]


displayCurrentModal : Model -> Html Msg
displayCurrentModal model =
    case model.modalIsVisible of
        True ->
            model.currentModal

        False ->
            div [] []


displayButtons : Html Msg
displayButtons =
    div [ class "buttons" ]
        [ displayCreateRecipeButton
        , displayImportButton
        , displayDownloadCollectionButton
        , displayEdamamAttribution
        ]


displayCreateRecipeButton : Html Msg
displayCreateRecipeButton =
    button [ class "button margin-right-1", onClick CreateNewRecipe ] [ text "Create Recipe" ]


displayImportButton : Html Msg
displayImportButton =
    button [ class "button margin-right-1", onClick ImportRecipe ] [ text "Import Recipe/Collection" ]


displayDownloadCollectionButton : Html Msg
displayDownloadCollectionButton =
    button [ class "button margin-right-1", onClick DownloadCollection ] [ text "Download Collection" ]


displayRecipeList : List Recipe -> Html Msg
displayRecipeList recipes =
    div [ class "margin-bottom-1" ] (List.map displayRecipeListItem recipes)


displayRecipeListItem : Recipe -> Html Msg
displayRecipeListItem recipe =
    span [ class "field is-grouped" ]
        [ a [ class "margin-right-05 list-item button has-text-left is-fullwidth", onClick (OpenRecipe recipe) ] [ text recipe.title ]
        , button [ class "margin-right-05 button", onClick (EditRecipe recipe) ] [ i [ class "icon far fa-edit" ] [] ]
        , button [ class "button is-danger", onClick (OpenDeleteConfirmationModal recipe) ] [ i [ class "icon far fa-trash-alt" ] [] ]
        ]


displayRecipePicture : Recipe -> Html msg
displayRecipePicture recipe =
    section [] [ img [ src recipe.pictureURI, class "image" ] [] ]


displayIngredients : List String -> Html Msg
displayIngredients ingredients =
    section [ class "margin-top-1" ] [ h1 [ class "subtitle" ] [ text "Ingredients" ], div [ class "content" ] [ ul [] (List.map (\ingredient -> li [] [ text ingredient ]) ingredients) ] ]


displayIngredientsInEdit : EditMode -> List String -> Html Msg
displayIngredientsInEdit editMode ingredients =
    section [ class "margin-top-1" ] [ h1 [ class "subtitle" ] [ text "Ingredients" ], div [ class "content" ] [ ul [] (List.map (\ingredient -> li [] [ text ingredient, button [ class "button  is-small margin-left-05", onClick (Delete editMode (Ingredient ingredient)) ] [ i [ class "icon fas fa-minus" ] [] ] ]) ingredients) ] ]


displayInstructions : List String -> Html Msg
displayInstructions instructions =
    section [ class "margin-top-1 margin-bottom-1" ] [ h1 [ class "subtitle" ] [ text "Instruction" ], div [ class "content" ] [ ol [] (List.map (\instruction -> li [] [ text instruction ]) instructions) ] ]


displayInstructionsInEdit : EditMode -> List String -> Html Msg
displayInstructionsInEdit editMode instructions =
    section [ class "margin-top-1" ] [ h1 [ class "subtitle" ] [ text "Instruction" ], div [ class "content" ] [ ol [] (List.map (\instruction -> li [] [ text instruction, button [ class "button is-small margin-left-05", onClick (Delete editMode (Instruction instruction)) ] [ i [ class "icon fas fa-minus" ] [] ] ]) instructions) ] ]



-- generic helper for no bubbling up on click


onClickNoBubblingUp : msg -> Html.Attribute msg
onClickNoBubblingUp message =
    Html.Events.custom "click" (Json.Decode.succeed { message = message, stopPropagation = True, preventDefault = True })
