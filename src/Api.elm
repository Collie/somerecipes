module Api exposing (baseAPIURL, createAPIURL, getRidOfSpaces, sendAPIRequest)

import Http
import Json.Decode
import Msg exposing (..)


sendAPIRequest : String -> Cmd Msg
sendAPIRequest ingredient =
    Http.get
        { url = createAPIURL ingredient
        , expect = Http.expectJson GotText calorieDecoder
        }


createAPIURL : String -> String
createAPIURL input =
    baseAPIURL ++ getRidOfSpaces input


baseAPIURL : String
baseAPIURL =
    "https://api.edamam.com/api/nutrition-data?app_id=4701571e&app_key=2831ea0a4ad83a3a5ae251580917efba&ingr="


calorieDecoder : Json.Decode.Decoder String
calorieDecoder =
    Json.Decode.map String.fromInt (Json.Decode.field "calories" Json.Decode.int)


getRidOfSpaces : String -> String
getRidOfSpaces input =
    String.replace " " "+" input
