module Main exposing (main)

import Api
import Browser
import Display
import File exposing (File)
import File.Download as Download
import File.Select as Select
import Html exposing (Html, a, button, div, figure, footer, h1, h2, header, hr, i, img, input, label, li, main_, nav, ol, p, section, span, text, ul)
import Html.Attributes exposing (class, id, placeholder, src, style, type_, value)
import Html.Events exposing (onClick, onInput)
import Json.Encode
import List
import List.Extra
import Model exposing (..)
import Msg exposing (..)
import Recipe exposing (Recipe)
import Task


main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


init : () -> ( Model, Cmd Msg )
init _ =
    ( { recipeList = []
      , modalIsVisible = False
      , currentModal = div [] []
      , inputRecipe = Recipe.invalidRecipe
      , inputInstruction = ""
      , inputIngredient = ""
      , idCounter = 0
      , exportedRecipe = ""
      , importedFile = Nothing
      , viewMode = CardView
      }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        CloseModal ->
            ( model
                |> setModalVisibility False
                |> emptyAllInput
            , Cmd.none
            )

        Save editMode component ->
            case component of
                RecipeObject recipe ->
                    case Recipe.validateRecipe model.inputRecipe of
                        False ->
                            ( model, Cmd.none )

                        True ->
                            case editMode of
                                ChangeOld ->
                                    ( model
                                        |> updateRecipe
                                        |> emptyInputRecipe
                                        |> emptyModal
                                        |> setModalVisibility False
                                    , Cmd.none
                                    )

                                CreateNew ->
                                    ( model
                                        |> makeNewID
                                        |> addToRecipeList model.recipeList [ model.inputRecipe ]
                                        |> increaseCounter 1
                                        |> emptyInputRecipe
                                        |> emptyModal
                                        |> setModalVisibility False
                                    , Cmd.none
                                    )

                Instruction instruction ->
                    ( model
                        |> addInstructionToInputRecipe model.inputInstruction
                        |> emptyInputInstruction
                        |> setModal (Display.createRecipeModal (model |> addInstructionToInputRecipe model.inputInstruction) editMode)
                    , Cmd.none
                    )

                Ingredient ingredient ->
                    ( model
                        |> addIngredientToInputRecipe model.inputIngredient
                        |> emptyInputIngredient
                        |> setModal (Display.createRecipeModal (model |> addIngredientToInputRecipe model.inputIngredient) editMode)
                    , Api.sendAPIRequest model.inputIngredient
                    )

        CreateNewRecipe ->
            ( model
                |> setModalVisibility True
                |> setModal (Display.createRecipeModal model CreateNew)
            , Cmd.none
            )

        EditRecipe recipe ->
            ( model
                |> setInputRecipe recipe
                |> setModal (Display.createRecipeModal (model |> setInputRecipe recipe) ChangeOld)
                |> setModalVisibility True
            , Cmd.none
            )

        ToggleRecipeView newViewMode ->
            ( { model | viewMode = newViewMode }, Cmd.none )

        Delete editMode component ->
            case component of
                RecipeObject recipe ->
                    ( { model
                        | recipeList = List.Extra.remove recipe model.recipeList
                        , idCounter = model.idCounter - 1
                      }
                        |> setModal (div [] [])
                        |> setModalVisibility False
                    , Cmd.none
                    )

                Instruction instruction ->
                    let
                        changedModel =
                            model |> removeInstruction instruction
                    in
                    ( changedModel
                        |> setModal (Display.createRecipeModal changedModel editMode)
                    , Cmd.none
                    )

                Ingredient ingredient ->
                    let
                        changedModel =
                            model |> removeIngredient ingredient
                    in
                    ( changedModel
                        |> setModal (Display.createRecipeModal changedModel editMode)
                    , Cmd.none
                    )

        ToggleModalAppearance visibility ->
            ( model
                |> setModalVisibility visibility
            , Cmd.none
            )

        SetModelModal modal ->
            ( { model | currentModal = modal }, Cmd.none )

        OpenRecipe recipe ->
            ( model
                |> setModalVisibility True
                |> setModal (Display.displayRecipeModal recipe)
            , Cmd.none
            )

        UpdateRecipeComponent component input ->
            case component of
                IngredientInput ->
                    ( { model | inputIngredient = input }, Cmd.none )

                RecipeTitleInput ->
                    let
                        changeTitle newtitle recipe =
                            { recipe | title = newtitle }
                    in
                    ( { model | inputRecipe = changeTitle input model.inputRecipe }, Cmd.none )

                RecipePictureURIInput ->
                    let
                        changePic newPic recipe =
                            { recipe | pictureURI = newPic }
                    in
                    ( { model | inputRecipe = changePic input model.inputRecipe }, Cmd.none )

                InstructionInput ->
                    ( { model | inputInstruction = input }, Cmd.none )

                ServingsInput ->
                    let
                        convertServings num =
                            case String.toInt num of
                                Just a ->
                                    a

                                Nothing ->
                                    1

                        changedRecipe recipe =
                            { recipe | servings = convertServings input }
                    in
                    ( { model | inputRecipe = changedRecipe model.inputRecipe }, Cmd.none )

        DownloadRecipe recipe ->
            let
                newModel =
                    { model | exportedRecipe = Json.Encode.encode 4 (Recipe.exportRecipe recipe) }
            in
            ( newModel, downloadFile newModel.exportedRecipe recipe.title )

        DownloadCollection ->
            ( model, downloadFile (Json.Encode.encode 4 (Recipe.exportRecipeList model.recipeList)) "RecipeCollection" )

        ImportRecipe ->
            ( model, Select.file [ "text/json" ] FileSelected )

        FileSelected file ->
            ( model, Task.perform FileLoaded (File.toString file) )

        FileLoaded loadedFileContent ->
            let
                changedModel =
                    model |> setImportedFile loadedFileContent
            in
            ( changedModel
                |> addToRecipeList changedModel.recipeList (Recipe.decodeRecipe changedModel.importedFile)
            , Cmd.none
            )

        OpenDeleteConfirmationModal recipe ->
            ( model
                |> setModal
                    (Display.deleteRecipeConfirmationModal recipe)
                |> setModalVisibility True
            , Cmd.none
            )

        GotText result ->
            case result of
                Ok cals ->
                    let
                        updateCalories recipe =
                            { recipe
                                | calorieCount =
                                    recipe.calorieCount
                                        + (case String.toInt cals of
                                            Just a ->
                                                a

                                            Nothing ->
                                                0
                                          )
                            }
                    in
                    ( { model | inputRecipe = updateCalories model.inputRecipe }, Cmd.none )

                Err _ ->
                    ( model, Cmd.none )


view : Model -> Html Msg
view model =
    Html.main_ []
        [ Display.displayNavbar
        , div [ class "box scrollbox" ]
            [ case model.viewMode of
                ListView ->
                    Display.displayRecipeList model.recipeList

                CardView ->
                    Display.displayRecipeCards model.recipeList
            ]
        , div [ class "margin-left-1" ]
            [ Display.displayButtons
            , Display.displayCurrentModal model
            ]
        ]


downloadFile : String -> String -> Cmd msg
downloadFile toDownload name =
    Download.string (name ++ ".json") "text/json" toDownload
