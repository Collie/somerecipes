module Recipe exposing (Recipe, addIngredient, addInstruction, changeRecipeWithSameID, decodeRecipe, exportRecipe, exportRecipeList, invalidRecipe, isList, oldRecipeDecoder, recipeDecoder, recipeListDecoder, setRecipePicture, validateIngredient, validateRecipe, validateRecipeList)

import Json.Decode
import Json.Encode
import List.Extra


type alias Recipe =
    { title : String
    , id : Int
    , servings : Int
    , pictureURI : String
    , ingredients : List String
    , instructions : List String
    , categories : List String
    , calorieCount : Int
    }


exportRecipe : Recipe -> Json.Encode.Value
exportRecipe recipe =
    Json.Encode.object
        [ ( "title", Json.Encode.string recipe.title )
        , ( "servings", Json.Encode.int recipe.servings )
        , ( "pictureURI", Json.Encode.string recipe.pictureURI )
        , ( "ingredients", Json.Encode.list Json.Encode.string recipe.ingredients )
        , ( "instructions", Json.Encode.list Json.Encode.string recipe.instructions )
        , ( "categories", Json.Encode.list Json.Encode.string recipe.categories )
        , ( "calorieCount", Json.Encode.int recipe.calorieCount )
        ]


exportRecipeList : List Recipe -> Json.Encode.Value
exportRecipeList recipeList =
    Json.Encode.object [ ( "recipes", Json.Encode.list exportRecipe recipeList ) ]


addIngredient : String -> Recipe -> Recipe
addIngredient ingredient recipe =
    { recipe
        | ingredients =
            case validateIngredient ingredient of
                True ->
                    (++) recipe.ingredients [ ingredient ]

                False ->
                    recipe.ingredients
    }


validateIngredient : String -> Bool
validateIngredient ingredient =
    case ingredient of
        "" ->
            False

        _ ->
            True



-- functions on Recipes


invalidRecipe : Recipe
invalidRecipe =
    { title = ""
    , ingredients = []
    , pictureURI = ""
    , instructions = []
    , id = 0
    , categories = []
    , servings = 1
    , calorieCount = 0
    }


changeRecipeWithSameID : Recipe -> List Recipe -> List Recipe
changeRecipeWithSameID recipe recipeList =
    let
        getIndex list =
            List.Extra.findIndex
                (\elem ->
                    if recipe.id == elem.id then
                        True

                    else
                        False
                )
                list
    in
    case getIndex recipeList of
        Just val ->
            List.Extra.setAt val recipe recipeList

        _ ->
            recipeList


setRecipePicture : String -> Recipe -> Recipe
setRecipePicture newPic recipe =
    { recipe | pictureURI = newPic }


addInstruction : String -> Recipe -> Recipe
addInstruction instruction recipe =
    { recipe | instructions = (++) recipe.instructions [ instruction ] }


validateRecipeList : List Recipe -> Bool
validateRecipeList list =
    if List.all (\val -> val) (List.map validateRecipe list) then
        True

    else
        False


validateRecipe : Recipe -> Bool
validateRecipe recipe =
    case recipe.title of
        "" ->
            False

        _ ->
            case recipe.ingredients of
                [] ->
                    False

                _ ->
                    case recipe.instructions of
                        [] ->
                            False

                        _ ->
                            True


decodeRecipe : Maybe String -> List Recipe
decodeRecipe recipesAsJson =
    let
        combinedDecoder =
            Json.Decode.oneOf [ recipeDecoder, oldRecipeDecoder ]
    in
    case recipesAsJson of
        Just recipes ->
            case isList recipes of
                True ->
                    case Json.Decode.decodeString recipeListDecoder recipes of
                        Ok a ->
                            a

                        Err _ ->
                            [ invalidRecipe ]

                False ->
                    case Json.Decode.decodeString combinedDecoder recipes of
                        Ok a ->
                            [ a ]

                        Err _ ->
                            [ invalidRecipe ]

        Nothing ->
            [ invalidRecipe ]


isList : String -> Bool
isList recipes =
    case Json.Decode.decodeString recipeListDecoder recipes of
        Ok _ ->
            True

        Err _ ->
            False


recipeListDecoder : Json.Decode.Decoder (List Recipe)
recipeListDecoder =
    Json.Decode.field "recipes" (Json.Decode.list recipeDecoder)


oldRecipeDecoder : Json.Decode.Decoder Recipe
oldRecipeDecoder =
    Json.Decode.map8 Recipe
        (Json.Decode.field "title" Json.Decode.string)
        (Json.Decode.succeed 0)
        (Json.Decode.succeed 1)
        (Json.Decode.field "pictureURI" Json.Decode.string)
        (Json.Decode.at [ "ingredients" ] (Json.Decode.list Json.Decode.string))
        (Json.Decode.at [ "instructions" ] (Json.Decode.list Json.Decode.string))
        (Json.Decode.succeed [])
        (Json.Decode.succeed 0)


recipeDecoder : Json.Decode.Decoder Recipe
recipeDecoder =
    Json.Decode.map8 Recipe
        (Json.Decode.field "title" Json.Decode.string)
        (Json.Decode.succeed 0)
        (Json.Decode.field "servings" Json.Decode.int)
        (Json.Decode.field "pictureURI" Json.Decode.string)
        (Json.Decode.at [ "ingredients" ] (Json.Decode.list Json.Decode.string))
        (Json.Decode.at [ "instructions" ] (Json.Decode.list Json.Decode.string))
        (Json.Decode.at [ "categories" ] (Json.Decode.list Json.Decode.string))
        (Json.Decode.field "calorieCount" Json.Decode.int)
