# Some Recipes
## Live Version: https://collie.gitlab.io/somerecipes
Create recipes, save them and then have them displayed nicely.



How to run: 

1. [install elm](https://guide.elm-lang.org/install.html)
2. compile
    ```bash
    cd somerecipes
    elm make src/Main.elm --output public/Main.js
    elm reactor
    ```
3. open on localhost:8000/public/index.html